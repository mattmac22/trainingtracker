from django.views.generic import TemplateView

from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render

from google.appengine.api import users
from django.http import HttpResponse
import datetime
import urllib

def main_page(request):
    return render(request, 'probemanager/home_page.html')


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)

# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

