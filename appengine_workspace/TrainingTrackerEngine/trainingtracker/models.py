from google.appengine.ext import db
from django.db import models
    
    
class OligoList(models.Model):
    sequence = models.CharField(max_length=500)
    good_sequence = models.BooleanField()
    def __str__(self):
        return "%s %s" % (self.sequence, self.good_sequence)
    
class InputFileList(models.Model):
    file_path = models.FileField()
    def __str__(self):
        return "%s %s" % (self.file_path)
    
class ProbeDesign(models.Model):
    input_files = models.OneToOneField(InputFileList)
    oligo_list = models.OneToOneField(OligoList)
    # Increment ID's are added implicitly added
    target_coding_type = models.CharField(max_length=50)
    target_rna_type = models.CharField(max_length=50)
    associated_design_ids = models.CommaSeparatedIntegerField(max_length=500)
    good_design = models.BooleanField()
    species_of_origin = models.CharField(max_length=50)
    species_designed_against = models.CharField(max_length=50)
    gene_name_common = models.CharField(max_length=100)
    gene_name_designator = models.CharField(max_length=100)
    designer_name = models.CharField(max_length=50)
    date_designed = models.DateField()
    wells_used = models.CharField(max_length=50)
    biosearch_order_number = models.IntegerField()
    matlab_command = models.TextField()
    sequence_file = models.FileField()
    order_file = models.FileField()
    biosearch_csv = models.FileField()
    
class ProbeStock:
    date_created = models.DateField()
    probe_design_id = models.IntegerField()
    used_oligo_ids = models.CommaSeparatedIntegerField(max_length=500)
    associated_set_ids = models.CommaSeparatedIntegerField(max_length=500)
    coupled_dye = models.CharField(max_length=100)
    tube_label_note = models.TextField() 
    notes = models.TextField()
    location = models.CharField(max_length=150)
    


    
    
    